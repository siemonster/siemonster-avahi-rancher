# docker run --rm -it --name py-test --net=host python:2 bash
FROM debian:jessie


RUN apt-get update \
    && apt-get install -y avahi-daemon avahi-utils sipcalc dbus \
    && mkdir -p /var/run/dbus

COPY ./entrypoint.sh /entrypoint.sh
COPY ./avahi-daemon.conf /etc/avahi/avahi-daemon.conf
COPY ./rancher.service /etc/avahi/services/rancher.service
RUN chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
