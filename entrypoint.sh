#!/bin/bash
set -e

dbus-daemon --system

avahi-daemon --debug &

EXPECT_NODES=${EXPECT_NODES:-5}
ENV_DIR=/env
mkdir -p ${ENV_DIR}

v4dec() {
        for i; do
                echo $i | {
                        IFS=./
                        read a b c d e
                        test -z "$e" && e=32
                        echo -n "$((a<<24|b<<16|c<<8|d)) $((-1<<(32-e))) "
                }
        done
}

v4test() {
        v4dec $1 $2 | {
                read addr1 mask1 addr2 mask2
                if (( (addr1&mask2) == (addr2&mask2) && mask1 >= mask2 )); then
                        #echo "$1 is in network $2"
                        return 0
                else
                        #echo "$1 is not in network $2"
                        return 1
                fi
        }
}

mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}


cdr2mask ()
{
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

calc_naetmask ()
{
    IFS=. read -r i1 i2 i3 i4 <<< "$1"
    IFS=. read -r m1 m2 m3 m4 <<< "$2"
    printf "%d.%d.%d.%d\n" "$((i1 & m1))" "$((i2 & m2))" "$((i3 & m3))" "$((i4 & m4))"
}

join_by()
{
    local IFS="$1"; shift; echo "$*";
}

containsElement () 
{
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

num_nodes=0
while (( "$num_nodes" < "$EXPECT_NODES" )); do
    echo "Waiting for all nodes... (${num_nodes} of ${EXPECT_NODES})"
    sleep 2
    output=$(avahi-browse _rancher._tcp -t |grep IPv4 |awk '{print $2}' |sort |uniq --count |sort -rn |head -1 | awk '{print $1}')
    num_nodes=${output:-0}
done
echo "Done."

IP_INTERFACE=$(avahi-browse _rancher._tcp -t |grep IPv4 |awk '{print $2}' |sort |uniq --count |sort -rn |head -1 |awk '{print $2}')
IP_ADDRESS=$(ip -4 addr show  ${IP_INTERFACE} | grep "inet\b" | head -1 | awk '{print $2}' | cut -d/ -f1)
CIDR_NOTAION=$(ip -4 addr show  ${IP_INTERFACE} | grep "inet\b" | head -1  | awk '{print $2}' | cut -d/ -f2)
NETMASK=$(calc_naetmask ${IP_ADDRESS} $(cdr2mask ${CIDR_NOTAION}))

echo "Detected IP:${IP_ADDRESS} Network:${NETMASK}/${CIDR_NOTAION} IF:${IP_INTERFACE}"
echo "NODE_IP_ADDRESS=${IP_ADDRESS}" > ${ENV_DIR}/self.env
echo "NODE_INTERFACE=${IP_INTERFACE}" >> ${ENV_DIR}/self.env
echo "NODE_NETMASK=${NETMASK}/${CIDR_NOTAION}" >> ${ENV_DIR}/self.env
echo "NODE_SYSTEMD_IP_ADDRESS=${IP_ADDRESS}/${CIDR_NOTAION}" >> ${ENV_DIR}/self.env

entire_ips=()
entire_names=()

# Get a list of all ip addresses for a given interface
IFS=$'\r\n' GLOBIGNORE='*' command eval  'local_ips=($(ip -4 addr show ${IP_INTERFACE} | grep "inet\b" | awk "{print \$2}" | cut -d/ -f1))'

for host_name in $(avahi-browse _rancher._tcp -t |awk '{print $4}' |sort |uniq); do
    cur_ip="255.255.255.255"
    until $(v4test ${cur_ip} ${NETMASK}/${CIDR_NOTAION}); do
        echo "Trying to get ip address for $host_name"
        sleep 1
        cur_ip=$(avahi-resolve-host-name -4 $host_name.local | awk '{print $2}')
    done

    # Find self hostname
    set +e
    containsElement "${cur_ip}" "${local_ips[@]}"
    if [ "$?" == "0" ]; then
        echo "Node name is: ${host_name}"
        echo "NODE_NAME=${host_name}" >> ${ENV_DIR}/self.env
        echo "NODE_IP_ADDRESSES_LIST=$(join_by , ${local_ips[@]})" >> ${ENV_DIR}/self.env
        #entire_ips+=("${IP_ADDRESS}")
    else
        entire_ips+=("${cur_ip}")
        entire_names+=("${host_name}")
    fi
    set -e
done
echo "ALL_NODES_IPS=$(join_by , ${entire_ips[@]})" > ${ENV_DIR}/all_nodes.env
echo "ALL_NODES_NAMES=$(join_by , ${entire_names[@]})" >> ${ENV_DIR}/all_nodes.env
